//
//  NegocioService.swift
//  MapaTestApp
//
//  Created by Kevin Belter on 12/13/16.
//  Copyright © 2016 KevinBelter. All rights reserved.
//

import Foundation

class NegocioService {
    func getAllNegocios() -> [Negocio] {
        return [
            Negocio(
                latitude: -34.607718,
                longitude: -58.373221,
                nombre: "Carrefour",
                barrio: "Microcentro",
                descuento: "50% de Descuento en compra de carne."
            ),
            Negocio(
                latitude: -34.6105037,
                longitude: -58.3793133,
                nombre: "Wallmart",
                barrio: "Microcentro",
                descuento: "30% de descuento pagando con Club la Nacion."
            ),
            Negocio(
                latitude: -34.610389,
                longitude:  -58.374968,
                nombre: "Apple Store",
                barrio: "Microcentro",
                descuento: "2x1 Los Martes de Febrero."
            ),
            Negocio(
                latitude: -34.6110248,
                longitude: -58.3738415,
                nombre: "Freddo",
                barrio: "Microcentro",
                descuento: "40% de descuento en crema de leche."
            ),
            Negocio(
                latitude: -34.6094883,
                longitude: -58.3728222,
                nombre: "Parrilla 83",
                barrio: "Microcentro",
                descuento: "10% de descuento si saltas alto."
            ),
            Negocio(
                latitude: -34.6079055,
                longitude: -58.3693329,
                nombre: "Cabaña las lilas",
                barrio: "Microcentro",
                descuento: "140% de descuento en compra de calamares."
            )
        ]
    }
}
