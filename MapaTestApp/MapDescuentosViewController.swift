//
//  MapDescuentosViewController.swift
//  MapaTestApp
//
//  Created by Kevin Belter on 12/13/16.
//  Copyright © 2016 KevinBelter. All rights reserved.
//

import UIKit
import MapKit

class MapDescuentoViewController: UIViewController {
    
    @IBOutlet weak var mapConDescuentos: MKMapView! {
        didSet {
            mapConDescuentos.delegate = self
        }
    }
    
    @IBOutlet weak var lblNombreNegocio: UILabel!
    @IBOutlet weak var lblZona: UILabel!
    @IBOutlet weak var lblDescuento: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let negocios = NegocioService().getAllNegocios()
        mapConDescuentos.addAnnotations(negocios)
        mapConDescuentos.showAnnotations(mapConDescuentos.annotations, animated: true)
        
        guard let annotation = mapConDescuentos.annotations.first else { return }
        mapConDescuentos.selectAnnotation(annotation, animated: true)
    }
    
    @IBAction func swipeDerecha(_ sender: UISwipeGestureRecognizer) {
        swipeIfShould(condition: { index in
            index >= self.mapConDescuentos.annotations.count - 1
        }) { index in
            index + 1
        }
    }
    
    @IBAction func swipeIzquierda(_ sender: UISwipeGestureRecognizer) {
        swipeIfShould(condition: { index in
            index <= 0
        }) { index in
            index - 1
        }
    }
    
    private func swipeIfShould(condition: (Int) -> Bool, finalIndex: (Int) -> (Int)) {
        guard let negocio = mapConDescuentos.selectedAnnotations.first as? Negocio else { return }
        
        guard let index = (mapConDescuentos.annotations.index { annotation in
            guard let negocioAnnotation = annotation as? Negocio else { return false }
            
            return negocioAnnotation == negocio
        }) else { return }
        
        if condition(index) { return }
        
        mapConDescuentos.deselectAnnotation(mapConDescuentos.annotations[index], animated: true)
        mapConDescuentos.selectAnnotation(mapConDescuentos.annotations[finalIndex(index)], animated: true)
    }
}

extension MapDescuentoViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let negocio = view.annotation as? Negocio else { return }
        
        lblNombreNegocio.text = negocio.nombre
        lblZona.text = negocio.barrio
        lblDescuento.text = negocio.descuento.uppercased()
        
        view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        guard let _ = view.annotation as? Negocio else { return }
        
        view.transform = CGAffineTransform.identity
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        var annotationView: MKAnnotationView
        if let annView = mapView.dequeueReusableAnnotationView(withIdentifier: "Negocio") {
            annotationView = annView
            annotationView.annotation = annotation
        } else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "Negocio")
            annotationView.image = UIImage(named: "Gift")
        }
        
        return annotationView
    }
}
