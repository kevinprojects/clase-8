//
//  Negocio.swift
//  MapaTestApp
//
//  Created by Kevin Belter on 12/13/16.
//  Copyright © 2016 KevinBelter. All rights reserved.
//

import Foundation
import MapKit

class Negocio: NSObject {
    var latitude: Double
    var longitude: Double
    var nombre: String
    var barrio: String
    var descuento: String
    
    init(latitude: Double, longitude: Double, nombre: String, barrio: String, descuento: String) {
        self.latitude = latitude
        self.longitude = longitude
        self.nombre = nombre
        self.barrio = barrio
        self.descuento = descuento
    }
}

extension Negocio: MKAnnotation {
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(
            latitude: self.latitude,
            longitude: self.longitude)
    }
    var title: String? { return self.nombre }
    var subtitle: String? { return self.barrio }
}
